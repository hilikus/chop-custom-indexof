/**
 * Created by paul on 31/12/14.
 */


function chop(num, list){
  return list.indexOf(num);
}

function chop1(num, list){
  var i = 0;
  var result = -1;
  while(i < list.length){
    if(num == list[i]){
      result = i;
    }
    i++;
  }
  return result;
}


function chop2(num, list){
  var middle =Math.round(list.length/2);
  var counter=0;
  while(list[middle]!=num && list.length>1) {
    if(list[middle] < num) {
      //right half
      list=list.splice(middle);
      counter=counter+middle;
    } else {
      //left half
      list=list.splice(0,middle);
    }
    middle=Math.floor(list.length/2);
  }
  if( list.length <= 1 && list[middle]!=num){
    return -1;
  }
  return (middle+counter);
}
