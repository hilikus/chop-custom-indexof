# README #

This is a quick coding challenge to come up with a way to return an integer's position in an array - without using the built in indexOf method.

### Expected behaviour ###

given an integer and an array containing integers in numerical order, return the position of the integer in the array. If the integer isn't in the array return -1.

see the tests in chopSpec.js

### Code explained ###

chop.js - This code contains 3 different implementations so I could compare the time required to process each request.

The first method is the disallowed indexOf function to use as a benchmark.

The second method was my first attempt - the simplist solution which checks each item of the array in order looking for a match. This is very straight forward but would not be very efficient given a large array.

The third method is more efficient - given the array is in numerical order, it splits the in half and compare the integer we're looking for to the array item in the middle to see if it's larger or smaller. It then loops through this splitting and comparing method until it finds a match.

The third method is more suitable for large arrays as half the data is discounted after each comparision.